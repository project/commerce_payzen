<?php
/**
 * PayZen V2-Payment Module version 1.2.0 for Drupal_Commerce 7.x-1.x. Support contact : support@payzen.eu.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author    Lyra Network (http://www.lyra-network.com/)
 * @copyright 2014-2017 Lyra Network and contributors
 * @license   http://www.gnu.org/licenses/gpl.html  GNU General Public License (GPL v3)
 * @category  payment
 * @package   payzen
 */

/**
 * Implements hook_uninstall().
 */
function commerce_payzen_uninstall() {
    commerce_delete_fields('commerce_payzen_standard');
    commerce_delete_fields('commerce_payzen_multi');
    commerce_delete_fields('commerce_payzen_paypal');

    rules_config_delete(array('commerce_payment_payzen_standard'));
    rules_config_delete(array('commerce_payment_payzen_multi'));
    rules_config_delete(array('commerce_payment_payzen_paypal'));

    if (db_table_exists('locales_source')) {
        // find ids of locales to delete
        $res_ls = db_select('locales_source', 'l_s')
                        ->fields('l_s', array('lid'))
                        ->condition('context', 'commerce_payzen', '=')
                        ->execute();

        $lids = array();
        foreach ($res_ls as $val) {
            $lids[] = $val->lid;
        }

        if (! empty($lids)) {
            // delete module translation targets
            $req = 'DELETE FROM {locales_target} WHERE lid IN (' . implode(',', $lids) . ')';
            db_query($req);

            // delete module translation sources
            $req = 'DELETE FROM {locales_source} WHERE lid IN (' . implode(',', $lids) . ')';
            db_query($req);
        }
    }

    // clear the necessary caches and rebuild the menu items
    entity_defaults_rebuild();
    rules_clear_cache();
    menu_rebuild();
}
